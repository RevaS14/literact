<input type="hidden" name="lesson_type" value="other-iframe">

<div class="form-group">
    <label><?php echo get_phrase('iframe_source'); ?>( <?php echo get_phrase('provide_the_source_only'); ?> )</label>
    <textarea id = "iframe_source" name = "iframe_source" class="form-control"><?php echo $lesson_details['attachment']; ?></textarea>
</div>

<div class="form-group">
    <label><?php echo get_phrase('duration'); ?>( <?php echo get_phrase('for_web_application'); ?> )</label>
    <input type="text" name = "duration" id = "duration" value="<?php echo $lesson_details['duration']; ?>" class="form-control" required>
</div>
