<section class="home-banner-area" style="background-image: url('<?= base_url("uploads/system/" . get_frontend_settings('banner_image')); ?>');
        background-position: center top;
        background-size: cover;
        background-repeat: no-repeat;
        padding: 170px 0 130px;
        color: #fff;">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <div class="home-banner-wrap">
                    <h2><?php echo site_phrase(get_frontend_settings('banner_title')); ?></h2>
                    <p><?php echo site_phrase(get_frontend_settings('banner_sub_title')); ?></p>
                    <form class="" action="<?php echo site_url('home/search'); ?>" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="query" placeholder="<?php echo site_phrase('what_do_you_want_to_learn'); ?>?">
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-fact-area">
    <div class="container-lg">
        <div class="row">
            <?php $courses = $this->crud_model->get_courses(); ?>
            <div class="col-md-4 d-flex">
                <div class="home-fact-box mr-md-auto mr-auto">
                    <i class="fas fa-bullseye float-left"></i>
                    <div class="text-box">
                        
                        <h4><?php
                            $status_wise_courses = $this->crud_model->get_status_wise_courses();
                            $number_of_courses = $status_wise_courses['active']->num_rows();
                            echo $number_of_courses . ' ' . site_phrase('online_courses'); ?></h4>
                        <p><?php echo site_phrase('explore_a_variety_of_fresh_topics'); ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex">
                <div class="home-fact-box mr-md-auto mr-auto">
                    <i class="fa fa-check float-left"></i>
                    <div class="text-box">
                        <h4><?php echo site_phrase('expert_instruction'); ?></h4>
                        <p><?php echo site_phrase('find_the_right_course_for_you'); ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex">
                <div class="home-fact-box mr-md-auto mr-auto">
                    <i class="fa fa-clock float-left"></i>
                    <div class="text-box">
                        <h4><?php echo site_phrase('lifetime_access'); ?></h4>
                        <p><?php echo site_phrase('learn_on_your_schedule'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-carousel-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h2 class="course-carousel-title"><?php echo site_phrase('top_courses'); ?></h2>

                <!-- Animated page loader -->
                <?php include "animated-page-loader.php"; ?>

                <div class="course-carousel shown-after-loading" style="display: none;">
                    <?php $top_courses = $this->crud_model->get_top_courses()->result_array();
                    $cart_items = $this->session->userdata('cart_items');
                    foreach ($top_courses as $top_course) : ?>
                        <div class="course-box-wrap">
                            <a href="<?php echo site_url('home/course/' . rawurlencode(slugify($top_course['title'])) . '/' . $top_course['id']); ?>" class="has-popover">
                                <div class="course-box">
                                    <div class="course-image">
                                        <img src="<?php echo $this->crud_model->get_course_thumbnail_url($top_course['id']); ?>" alt="" class="img-fluid">
                                    </div>
                                    <div class="course-details">
                                        <h5 class="title"><?php echo $top_course['title']; ?></h5>
                                        <p class="instructors"><?php echo $top_course['short_description']; ?></p>
                                        <div class="rating">
                                            <?php
                                            $total_rating =  $this->crud_model->get_ratings('course', $top_course['id'], true)->row()->rating;
                                            $number_of_ratings = $this->crud_model->get_ratings('course', $top_course['id'])->num_rows();
                                            if ($number_of_ratings > 0) {
                                                $average_ceil_rating = ceil($total_rating / $number_of_ratings);
                                            } else {
                                                $average_ceil_rating = 0;
                                            }

                                            for ($i = 1; $i < 6; $i++) : ?>
                                                <?php if ($i <= $average_ceil_rating) : ?>
                                                    <i class="fas fa-star filled"></i>
                                                <?php else : ?>
                                                    <i class="fas fa-star"></i>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                            <span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span>
                                        </div>
                                        <div class="d-block">
                                            <p class="text-left text-secondary d-inline-block course-compare" style="font-size: 13px; cursor : pointer; font-weight : 500; color : #4d98ad !important;" redirect_to="<?php echo site_url('home/compare?course-1=' . rawurlencode(slugify($top_course['title'])) . '&&course-id-1=' . $top_course['id']); ?>">
                                                <i class="fas fa-balance-scale"></i> <?php echo site_phrase('compare'); ?>
                                            </p>
                                            <?php if ($top_course['is_free_course'] == 1) : ?>
                                                <p class="price text-right d-inline-block float-right"><?php echo site_phrase('free'); ?></p>
                                            <?php else : ?>
                                                <?php if ($top_course['discount_flag'] == 1) : ?>
                                                    <p class="price text-right d-inline-block float-right"><small><?php echo currency($top_course['price']); ?></small><?php echo currency($top_course['discounted_price']); ?></p>
                                                <?php else : ?>
                                                    <p class="price text-right d-inline-block float-right"><?php echo currency($top_course['price']); ?></p>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <div class="webui-popover-content">
                                <div class="course-popover-content">
                                    <?php if ($top_course['last_modified'] == "") : ?>
                                        <div class="last-updated"><?php echo site_phrase('last_updated') . ' ' . date('D, d-M-Y', $top_course['date_added']); ?></div>
                                    <?php else : ?>
                                        <div class="last-updated"><?php echo site_phrase('last_updated') . ' ' . date('D, d-M-Y', $top_course['last_modified']); ?></div>
                                    <?php endif; ?>

                                    <div class="course-title">
                                        <a href="<?php echo site_url('home/course/' . rawurlencode(slugify($top_course['title'])) . '/' . $top_course['id']); ?>"><?php echo $top_course['title']; ?></a>
                                    </div>
                                    <div class="course-meta">
                                        <?php if ($top_course['course_type'] == 'general') : ?>
                                            <span class=""><i class="fas fa-play-circle"></i>
                                                <?php echo $this->crud_model->get_lessons('course', $top_course['id'])->num_rows() . ' ' . site_phrase('lessons'); ?>
                                            </span>
                                            <span class=""><i class="far fa-clock"></i>
                                                <?php
                                                $total_duration = 0;
                                                $lessons = $this->crud_model->get_lessons('course', $top_course['id'])->result_array();
                                                foreach ($lessons as $lesson) {
                                                    if ($lesson['lesson_type'] != "other" && $lesson['lesson_type'] != "text") {
                                                        $time_array = explode(':', $lesson['duration']);
                                                        $hour_to_seconds = $time_array[0] * 60 * 60;
                                                        $minute_to_seconds = $time_array[1] * 60;
                                                        $seconds = $time_array[2];
                                                        $total_duration += $hour_to_seconds + $minute_to_seconds + $seconds;
                                                    }
                                                }
                                                echo gmdate("H:i:s", $total_duration) . ' ' . site_phrase('hours');
                                                ?>
                                            </span>
                                        <?php elseif ($top_course['course_type'] == 'scorm') : ?>
                                            <span class="badge badge-light"><?= site_phrase('scorm_course'); ?></span>
                                        <?php endif; ?>
                                        <span class=""><i class="fas fa-closed-captioning"></i><?php echo ucfirst($top_course['language']); ?></span>
                                    </div>
                                    <div class="course-subtitle"><?php echo $top_course['short_description']; ?></div>
                                    <div class="what-will-learn">
                                        <ul>
                                            <?php
                                            $outcomes = json_decode($top_course['outcomes']);
                                            foreach ($outcomes as $outcome) : ?>
                                                <li><?php echo $outcome; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <div class="popover-btns">
                                        
                                        <?php if (is_purchased($top_course['id'])) : ?>
                                            <div class="purchased">
                                                <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo site_phrase('already_purchased'); ?></a>
                                            </div>
                                        <?php else : ?>
                                            <?php if ($top_course['is_free_course'] == 1) :
                                                if ($this->session->userdata('user_login') != 1) {
                                                    $url = "#";
                                                } else {
                                                    $url = site_url('home/get_enrolled_to_free_course/' . $top_course['id']);
                                                } ?>
                                                <a href="<?php echo $url; ?>" class="btn add-to-cart-btn big-cart-button" onclick="handleEnrolledButton()"><?php echo site_phrase('get_enrolled'); ?></a>
                                            <?php else : ?>
                                                <button type="button" class="btn add-to-cart-btn <?php if (in_array($top_course['id'], $cart_items)) echo 'addedToCart'; ?> big-cart-button-<?php echo $top_course['id']; ?>" id="<?php echo $top_course['id']; ?>" onclick="handleCartItems(this)">
                                                    <?php
                                                    if (in_array($top_course['id'], $cart_items))
                                                        echo site_phrase('added_to_cart');
                                                    else
                                                        echo site_phrase('add_to_cart');
                                                    ?>
                                                </button>
                                            <?php endif; ?>
                                            <button type="button" class="wishlist-btn <?php if ($this->crud_model->is_added_to_wishlist($top_course['id'])) echo 'active'; ?>" title="Add to wishlist" onclick="handleWishList(this)" id="<?php echo $top_course['id']; ?>"><i class="fas fa-heart"></i></button>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
  .carousel-inner img {width: 85%; max-height: 260px;border-radius: 10px;}
/* Carousel Header Styles */
.header-text {
    position: absolute;
    top: 35%;
    left: 1.8%;
    right: auto;
    width: 96.66666666666666%;
    color: #e2e2e2;
    font-size:20px; letter-spacing:1px;
}
.header-text h2 {font-size: 50px; color:#fff; font-weight:600;}
.header-text h2 span {padding: 10px;color:orange;}
.header-text h3 span {padding: 15px;}
.carousel-indicators li:hover {
    cursor:pointer;
}

</style>


    
<!--certificate section -->
  <section class="pt-5 pb-5 certificate-section">
        <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-sm-6 img-container">
                <div class="img-section">
                     <img src='<?= base_url("assets/frontend/default/img/banner_2.png"); ?>' />
                </div>
            </div>
             <div class="col-12 col-sm-6">
                <div class="content-section">
                    <h1 class="titlw">
                        Get the Literact advantage
                    </h1>
                    <ul class="certificate-point">
                         <li><i class="far fa-check-circle"></i> Conceptual clarity through visualisation </li>
                         <li><i class="far fa-check-circle"></i> Personalised learning programs </li> 
                         <li><i class="far fa-check-circle"></i> Unmatched individual attention </li>
                         <li><i class="far fa-check-circle"></i> Native language advantage </li>
                         <li><i class="far fa-check-circle"></i> Experts career guidance </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
        
        
        <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
  <defs>
        <filter id="round">
            <feGaussianBlur in="SourceGraphic" stdDeviation="5" result="blur" />    
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
            <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
        </filter>
    </defs>
</svg>
        
        
  </section>




<!--certificate section -->
  <section class="pt-5 pb-5 certificate-section">
        <div class="container">
        <div class="row align-items-center">
           
             <div class="col-12 col-sm-6">
                <div class="content-section">
                    <h1 class="titlw">
                        Become an Instructor
                    </h1>
                    <p>
                        Instructors from around the world tech millions of students on Udemy.We provide the tools and skills to teach that you love.
                    </p>
                    <ul class="certificate-point">
                         <li><i class="far fa-check-circle"></i> Challenge </li>
                         <li><i class="far fa-check-circle"></i> Award Certificate </li> 
                         <li><i class="far fa-check-circle"></i> Expertise License </li>
                    </ul>
                    <a href="<?php echo base_url('home/sign_up'); ?>" class="btn btn-sign-up">Sign Up</a>
                </div>
            </div>
             <div class="col-12 col-sm-6 img-container">
                <div class="img-section">
                     <img src='<?= base_url("assets/frontend/default/img/banner.png"); ?>' />
                </div>
            </div>
        </div>
        </div>
        
        
        <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
  <defs>
        <filter id="round">
            <feGaussianBlur in="SourceGraphic" stdDeviation="5" result="blur" />    
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
            <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
        </filter>
    </defs>
</svg>
        
        
  </section>

<style>
.certificate-section{
    background:#fff;
}

    .img-section{
       position: relative;
       }
        .img-section{
             /*clip-path: polygon(50% 0%, 10% 100%, 90% 100%);*/
            /*width: 100%;*/
            /* filter:url(#round);*/
        }
        .img-section img{
            width: 100%;
            /*clip-path: polygon(50% 0%, 10% 100%, 90% 100%);*/
        }
        
        .img-section::before {
         content: "";
        }

.content-section h1{
    font-size: 40px;
    font-weight: 900;
}
.certificate-point{
    padding: 0;
    list-style-type: none;
}
.certificate-point li{
    font-size: 20px;
    padding-bottom: 10px;
}
.certificate-point li i{
    color: #6862af;
}
</style>










<!--<section>-->
<!--    <div class="container-lg">-->
<!--        <div class="row">-->
<!--            <div class="col" style="text-align:center">-->

<!--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">-->
<!--        		<ol class="carousel-indicators">-->
<!--        			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>-->
<!--        			<li data-target="#carousel-example-generic" data-slide-to="1"></li>-->
<!--        			<li data-target="#carousel-example-generic" data-slide-to="2"></li>-->
<!--        		</ol>-->

<!--        		<div class="carousel-inner" role="listbox">-->
<!--        			<div class="carousel-item active">-->
<!--        			    <a href="#">-->
<!--        				<img src="https://azoom-sites.rockthemes.net/abboxed/wp-content/uploads/sites/14/2015/05/abboxed-restaurant-portfolio2.jpg" alt="First Slide" />-->
<!--        				</a>-->
<!--        			</div>-->

<!--        			<div class="carousel-item">-->
<!--        			    <a href="#">-->
<!--        				<img src="https://azoom-sites.rockthemes.net/abboxed/wp-content/uploads/sites/14/2015/05/abboxed-beach-portfolio.jpg" alt="Second Slide" />-->
<!--        				</a>-->

        			
<!--        			</div>-->

<!--        			<div class="carousel-item">-->
<!--        			    <a href="#">-->
<!--        				<img src="https://azoom-sites.rockthemes.net/abboxed/wp-content/uploads/sites/14/2015/05/abboxed-beach-portfolio2.jpg" alt="Third Slide" />-->
<!--        				</a>-->
<!--        			</div>-->
<!--        		</div>-->

<!--        		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">-->
<!--        			<span class="icon-prev" aria-hidden="true"></span>-->
<!--        			<span class="sr-only">Previous</span>-->
<!--        		</a>-->

<!--        		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">-->
<!--        			<span class="icon-next" aria-hidden="true"></span>-->
<!--        			<span class="sr-only">Next</span>-->
<!--        		</a>-->
<!--        	</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->







<!--start trial section -->
<section class="trial-section">

<div class="container-lg">
    <div class="row">
        <div class="col-12">
            <div class="success-content text-center">
            <h1>Find the right learning path for you</h1>
            <p>Match your goals to our programs, explore your options and <br/> map out your path to success</p>
            <a class="trial-btn btn">Start trial </a>
            </div>
        </div>
    </div>
</div>    
    
    <div class="cl-1"><img src='<?= base_url("assets/frontend/default/img/client1.jpg"); ?>' /></div>
    <div class="cl-2"><img src='<?= base_url("assets/frontend/default/img/client2.jpg"); ?>' /></div>
    <div class="cl-3"><img src='<?= base_url("assets/frontend/default/img/client3.jpg"); ?>' /></div>
    <div class="cl-4"><img src='<?= base_url("assets/frontend/default/img/client4.jpg"); ?>' /></div>
    
</section>

<style>
.cl-1{
    position: absolute;
    top: 0;
     left:0;
    width: 200px;
    height: 200px;
    border-radius: 100%;
    overflow: hidden;
    background: #6862af;
    box-shadow: 1px 1px 13px 0px #00000045;
}
.cl-1 img{
    object-fit: contain;
    width: 200px;
    height: 200px;
    padding: 20px;
    border-radius: 100%;
    
}
.cl-2{
   position: absolute;
    left: 11%;
    bottom: 0;
    width: 250px;
    height: 250px;
    border-radius: 100%;
    overflow: hidden;
    background: #6862af;
    box-shadow: 1px 1px 13px 0px #00000045;
}
.cl-2 img{
     object-fit: contain;
    width: 250px;
    height: 250px;
    padding: 30px;
    border-radius: 100%;
}
.cl-3{
     position: absolute;
    top: 30px;
    right: 70px;
    width: 200px;
    height: 200px;
    border-radius: 100%;
    overflow: hidden;
    background: #6862af;
    box-shadow: 1px 1px 13px 0px #00000045;
}
.cl-3 img{
     object-fit: contain;
    width: 200px;
    height: 200px;
    padding: 25px;
    border-radius: 100%;
}
.cl-4{
      position: absolute;
    bottom: 30px;
    right: 16%;
    width: 150px;
    height: 150px;
    border-radius: 100%;
    overflow: hidden;
    background: #6862af;
    box-shadow: 1px 1px 13px 0px #00000045;
}
.cl-4 img{
     object-fit: contain;
    width: 150px;
    height: 150px;
    padding: 20px;
    border-radius: 100%;
}
.trial-section{
    padding: 10rem 0px;
    background: #6862AF;
    color: #fff;
    position: relative;
    overflow: hidden;
}
.trial-section:before{
    content:"";
    background: #ffffff47;
    width: 300px;
    height: 300px;
    position: absolute;
    bottom: -30px;
    left: -90px;
    border-radius: 100%;
    box-shadow: -30px -30px 0px 60px #928ec545;
    
}
.trial-section:after{
    content:"";
   background: #ffffff47;
    width: 300px;
    height: 300px;
    position: absolute;
    top: -180px;
    right: 20%;
    border-radius: 100%;
    box-shadow: 0px -20px 0px 60px #928ec545;
    
}

    .success-content h1{
        font-size: 48px;
    font-weight: 700;
    }
     .success-content p{
        
    }
    .trial-btn, .trial-btn:hover{
        background: #FF8F00;
    border-color: #FF8F00;
    color: #fff !important;
    margin-top: 16px;
    border-radius: 8px;
    padding: 10px 60px;
    }
</style>



<script type="text/javascript">
    function handleWishList(elem) {

        $.ajax({
            url: '<?php echo site_url('home/handleWishList'); ?>',
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                } else {
                    if ($(elem).hasClass('active')) {
                        $(elem).removeClass('active')
                    } else {
                        $(elem).addClass('active')
                    }
                    $('#wishlist_items').html(response);
                }
            }
        });
    }

    function handleCartItems(elem) {
        url1 = '<?php echo site_url('home/handleCartItems'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                $('#cart_items').html(response);
                if ($(elem).hasClass('addedToCart')) {
                    $('.big-cart-button-' + elem.id).removeClass('addedToCart')
                    $('.big-cart-button-' + elem.id).text("<?php echo site_phrase('add_to_cart'); ?>");
                } else {
                    $('.big-cart-button-' + elem.id).addClass('addedToCart')
                    $('.big-cart-button-' + elem.id).text("<?php echo site_phrase('added_to_cart'); ?>");
                }
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                    }
                });
            }
        });
    }

    function handleEnrolledButton() {
        $.ajax({
            url: '<?php echo site_url('home/isLoggedIn'); ?>',
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                }
            }
        });
    }

    $(document).ready(function() {
        if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            if ($(window).width() >= 840) {
                $('a.has-popover').webuiPopover({
                    trigger: 'hover',
                    animation: 'pop',
                    placement: 'horizontal',
                    delay: {
                        show: 500,
                        hide: null
                    },
                    width: 330
                });
            } else {
                $('a.has-popover').webuiPopover({
                    trigger: 'hover',
                    animation: 'pop',
                    placement: 'vertical',
                    delay: {
                        show: 100,
                        hide: null
                    },
                    width: 335
                });
            }
        }
    });

    $('.course-compare').click(function(e) {
        e.preventDefault()
        var redirect_to = $(this).attr('redirect_to');
        window.location.replace(redirect_to);
    });
</script>